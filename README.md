# Goal is to test simulatenous run of different/heterogenous kernels and max nb of them

Usage
python generate_kernels.py -n 16
generates 16 different kernels, you need to move output file to src/

Modify aNb_kernels in host.cpp to specify nb of kernels to work on (buffers and scheduling in out of order execution queue will be done accordingly)

