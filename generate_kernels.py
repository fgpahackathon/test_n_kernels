#!/usr/bin/python
import argparse

template_kernel = """kernel __attribute__((reqd_work_group_size(1, 1, 1)))
void kernel{kernel_nb}(global int* out,
             global const int* in,
             const int nb_elems)
{{
    for(int i=0; i<nb_elems;++i)
    {{
        if(i%2)
        {{
            out[i]=in[i];
        }}
        else
        {{
            out[i]={kernel_value};
        }}
    }}
}}

"""

def generate(nb_kernels,default_output_kernel_filename):
    f = open(default_output_kernel_filename,'w')
    for i in range(nb_kernels):
        f.write(template_kernel.format(kernel_nb=i,kernel_value=40+i))
    f.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', '--nb_kernels', help='number of kernels to generate')
    args = parser.parse_args()

    if args.nb_kernels == None:
        print "Missing arg: needs nb of kernels. --help option."
        exit()
    
    default_output_kernel_filename = "n_kernels.cl"
    print "Generating to output kernel " + default_output_kernel_filename + "..."
    
    generate(int(args.nb_kernels),default_output_kernel_filename)
